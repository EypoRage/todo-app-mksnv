const koa = require('koa')
const mongoose = require('mongoose')
const convert = require('koa-convert')
const bodyParser = require('koa-bodyparser')
const router = require('koa-simple-router')
const error = require('koa-json-error')
const logger = require('koa-logger')
const koaRes = require('koa-res')
const serve = require('koa-static');
const mount = require('koa-mount');
const handleError = require('koa-handle-error')
const cors = require('koa-cors');
const todo = require('./controller/todo')
const app = new koa()
var server = require('http').createServer(app.callback())
const io = require('socket.io')(server);

/*
    Mongoose Config
*/
mongoose.Promise = require('bluebird')
mongoose
    .connect('mongodb://localhost/todoDB')
    .then((response) => {
        console.log('mongo connection created')
    })
    .catch((err) => {
        console.log("Error connecting to Mongo")
        console.log(err);
    })


/*
    Server Config
*/
// error handling
app.use(async(ctx, next) => {
    try {
        await next()
    } catch (err) {
        ctx.status = err.status || 500
        ctx.body = err.message
        ctx.app.emit('error', err, ctx)
    }
})


//CORS
app.use(cors());
// logging
app.use(logger())
    // body parsing
app.use(bodyParser())
    // format response as JSON
    //app.use(convert(koaRes()))
    // configure router
app.use(router(_ => {
    _.get('/api/saysomething', async(ctx) => {
            ctx.body = 'hello world'
        }),
        _.get('/api/throwerror', async(ctx) => {
            throw new Error('Aghh! An error!')
        }),
        _.get('/api/todos', todo.getTodos),
        _.post('/api/todo', todo.createTodo),
        _.put('/api/todo', todo.updateTodo),
        _.delete('/api/todo', todo.deleteTodo),
        _.post('/api/todo/multi', todo.createConcurrentTodos),
        _.delete('/api/todo/multi', todo.deleteConcurrentTodos)

}))

app.use(serve('view'));

io.on('connection', function(socket) {
    console.log('a user connected');
    socket.on('disconnect', function() {
        console.log('user disconnected');
    });
});

io.on('connection', function(socket) {
    socket.on('update', function(msg) {
        io.emit('update', msg);
        console.log('message: ' + msg);
    });
});


server.listen(80)