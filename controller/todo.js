const Todo = require('../model/Todo')
exports.getTodos = async(ctx) => {
    const todos = await Todo.find({})
    if (!todos) {
        throw new Error("There was an error retrieving your Todos.")
    } else {
        ctx.body = todos
    }
}




exports.createTodo = async(ctx) => {
    const result = await Todo.create({
        title: ctx.request.body.title,
        done: ctx.request.body.done
    })
    if (!result) {
        throw new Error('Todo failed to create.')
    } else {
        ctx.body = { message: 'Todo created!', data: result }
    }
}

exports.updateTodo = async(ctx) => {
    const searchByTitle = { title: ctx.request.body.title }
    const update = { title: ctx.request.body.newTitle, done: ctx.request.body.newDone }
    const result = await Todo.findOneAndUpdate(searchByTitle, update)
    if (!result) {
        throw new Error('Todo failed to update.')
    } else {
        console.log(result)
        ctx.body = { message: 'Todo updated!', data: result }
    }
}

exports.deleteTodo = async(ctx) => {
    const result = await Todo.findOneAndRemove({ title: ctx.request.body.title })
    if (!result) {
        throw new Error('Todo failed to delete.')
    } else {
        ctx.status = 200
        ctx.body = { message: 'success!' }

    }


}




exports.createConcurrentTodos = async(ctx) => {
    const todoOne = Todo.create({
        title: ctx.request.body.titleTodoOne,
        done: ctx.request.body.doneTodoOne
    })
    const todoTwo = Todo.create({
        title: ctx.request.body.titleTodoTwo,
        done: ctx.request.body.doneTodoTwo
    })
    const [t1, t2] = await Promise.all([todoOne, todoTwo])
    if (!t1 || !t2) {
        throw new Error('Todos failed to be created.')
    } else {
        ctx.body = { message: 'Todos created!', todoOne: t1, todoTwo: t2 }
    }
}


exports.deleteConcurrentTodos = async(ctx) => {
    const todoOne = Todo.findOneAndRemove({ title: ctx.request.body.titleTaskOne })
    const todoTwo = Todo.findOneAndRemove({ title: ctx.request.body.titleTaskTwo })
    const [t1, t2] = await Promise.all([todoOne, todoTwo])
    if (!t1 || !t2) {
        throw new Error('Todos failed to delete.')
    } else {
        ctx.body = { message: 'Todos deleted successfully!' }
    }
}