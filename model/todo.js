const mongoose = require('mongoose')
    /*
        Task Schema
    */
const TodoSchema = mongoose.Schema = {
    title: String,
    done: Boolean
}

module.exports = mongoose.model("Todo", TodoSchema)