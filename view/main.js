//hallo


var socket = io();


const header = {
    'crossDomain': true,
    'Content-Type': 'application/json'
}




Vue.component('page', {
    template: '#page'
})
var vm = new Vue({
    el: '#app',
    data: {
        clipped: true,
        drawer: true,
        fixed: false,
        miniVariant: false,

        title: 'TODO APP',
        todos: null,
        messages: []

    },
    sockets: {
        connect: function() {
            console.log('socket connected')
        },
        customEmit: function(val) {
            console.log('this method was fired by the socket server. eg: io.emit("customEmit", data)')
        }
    },
    methods: {
        deleteTodo: function(index, item) {

            axios.delete('http://localhost:80/api/todo', {
                data: {
                    title: item.title
                }
            }).then(res => {
                console.log(JSON.stringify(res))
                this.todos.splice(index, 1);
            });
            socket.emit('update', "1");
        },


        editTodo: function(index, item) {
            if (item.edit == null) {
                item.edit = true
                item.oldTitle = item.title
                socket.emit('update', "1");
            } else if (item.edit == true) {
                item.edit = false
                axios.put('http://localhost:80/api/todo', {
                    title: item.oldTitle,
                    newTitle: item.title
                });
                socket.emit('update', "1");
            } else if (item.edit == false) {
                item.edit = true;
                item.oldTitle = item.title
            }
            console.log(item.edit)


        },

        createTodo: function() {



            this.todos.push({
                "title": "neues Todo",
                "done": false,
                "edit": false
            })
            axios.post('http://localhost:80/api/todo', {
                title: "neues Todo",
                done: false
            })
            socket.emit('update', "1");



        },






    },
    mounted() {
        axios.get('http://localhost:80/api/todos', {
                header
            })
            .then(res => {
                console.log(JSON.stringify(res))

                res.data.forEach(todo => {
                    todo.edit = false
                });
                this.todos = res.data
            })

        socket.on('update', function(data) {
            console.log(data);
            console.log(vm.todos);
            axios.get('http://localhost:80/api/todos', {
                    header
                })
                .then(res => {
                    console.log(JSON.stringify(res))

                    res.data.forEach(todo => {
                        todo.edit = false
                    });
                    vm.todos = res.data
                })



        });




    }






});